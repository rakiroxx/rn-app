import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { useTranslation } from 'react-i18next';
import MainStack from './stacks/MainStack';
import CustomDrawerContent from './ui/organisms/CustomDrawerContent';

const Drawer = createDrawerNavigator();

export default () => {
  // This makes re-render on all App for translation change. I Don't like it
  const { t } = useTranslation('shared');
  return (
    <Drawer.Navigator
      initialRouteName="MainStack"
      drawerPosition="right"
      drawerContent={CustomDrawerContent}
    >
      <Drawer.Screen
        name={t('Create Issue')}
        component={MainStack}
      />
    </Drawer.Navigator>
  );
};
