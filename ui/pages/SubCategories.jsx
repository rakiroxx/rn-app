import React, { useLayoutEffect } from 'react';
import {
  StatusBar, StyleSheet, SafeAreaView, Linking,
} from 'react-native';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Icon } from 'react-native-elements';
import Mixpanel from 'react-native-mixpanel';
import ListLayout from '../templates/ListLayout';
import Faker from '../../faker/heyirys_covid.json';
import CategoryDescription from '../molecules/CategoryDescription';
import CategoryDetailList from '../organisms/CategoryDetailList';
import Text from '../atoms/Text';
import useCurrentLang from '../../hooks/useCurrentLang';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  menu: {
    marginRight: 10,
  },
});

const SubCategories = ({ navigation, route }) => {
  const { t } = useTranslation('subcategories');
  const currentLang = useCurrentLang();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => <Text bold>{t('Subcategory')}</Text>,
      headerRight: () => <Icon onPress={() => navigation.openDrawer()} name="menu" style={styles.menu} />,
      headerBackTitle: null,
    });
  }, [navigation, t]);

  const { categories } = Faker;
  const mapCategories = ([id, content]) => ({ id, ...content });
  const sortCategories = (c1, c2) => c1.order - c2.order;
  const path = route.params.path.split(',');
  const reduceToData = (acc, name, i) => ((i === 0) ? acc[name] : acc.subcategories[name]);
  const { subcategories, name, question } = path.reduce(reduceToData, categories);

  const onCategoryPress = (id) => {
    Mixpanel.trackWithProperties('Selected Subcategory', { id, path });
    const {
      subcategories: deepSubcategories,
      requireDescription,
      categoryType,
      url,
    } = subcategories[id];
    if (deepSubcategories) {
      navigation.navigate({ name: 'SubCategories', key: id, params: { path: `${path},${id}` } });
    } else if (requireDescription) {
      navigation.navigate({ name: 'DescriptionForm', params: { path: `${path},${id}` } });
    } else if (categoryType === 'redirect' && url) {
      Linking.openURL(url);
    } else {
      navigation.navigate({ name: 'MapForm', params: { path: `${path},${id}` } });
    }
  };

  let detail = t('Default Question');
  if (question) {
    detail = question[currentLang];
  }
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <ListLayout
        head={<CategoryDescription title={name[currentLang]} detail={detail} />}
        content={(
          <CategoryDetailList
            data={Object.entries(subcategories).map(mapCategories).sort(sortCategories)}
            onCategoryPress={onCategoryPress}
          />
        )}
      />
    </SafeAreaView>
  );
};

SubCategories.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
      path: PropTypes.string.isRequired,
    }),
  }).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    setOptions: PropTypes.func.isRequired,
    openDrawer: PropTypes.func.isRequired,
  }).isRequired,
};

export default SubCategories;
