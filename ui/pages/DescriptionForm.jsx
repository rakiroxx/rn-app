import React, { useState, useLayoutEffect } from 'react';
import { StatusBar, StyleSheet, SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Icon } from 'react-native-elements';
import Mixpanel from 'react-native-mixpanel';
import ListLayout from '../templates/ListLayout';
import Faker from '../../faker/heyirys_covid.json';
import CategoryDescription from '../molecules/CategoryDescription';
import DetailForm from '../molecules/DetailForm';
import Text from '../atoms/Text';
import useCurrentLang from '../../hooks/useCurrentLang';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  menu: {
    marginRight: 10,
  },
});

const SubCategories = ({ navigation, route }) => {
  const { t } = useTranslation(['descriptionform', 'shared']);
  const [detail, setDetail] = useState('');
  const currentLang = useCurrentLang();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => <Text bold>{t('Description Form')}</Text>,
      headerRight: () => <Icon onPress={() => navigation.openDrawer()} name="menu" style={styles.menu} />,
      headerBackTitle: null,
    });
  }, [navigation, t]);

  const { categories } = Faker;
  const path = route.params.path.split(',');
  const reduceToData = (acc, name, i) => ((i === 0) ? acc[name] : acc.subcategories[name]);
  const { name } = path.reduce(reduceToData, categories);

  const onFormSubmit = () => {
    navigation.navigate('MapForm', { path, detail });
    Mixpanel.trackWithProperties('Detail Form', { path });
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <ListLayout
        head={(
          <CategoryDescription
            title={name[currentLang]}
            detail={t('Provide Description')}
          />
        )}
        content={(
          <DetailForm
            limitChars={200}
            placeholder={t('Input Limit')}
            onChangeText={setDetail}
            value={detail}
            buttonLabel={t('shared:Next')}
            onButtonPress={onFormSubmit}
          />
        )}
      />
    </SafeAreaView>
  );
};

SubCategories.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
      path: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    setOptions: PropTypes.func.isRequired,
    openDrawer: PropTypes.func.isRequired,
  }).isRequired,
};

export default SubCategories;
