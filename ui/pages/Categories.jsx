import React, { useLayoutEffect, useEffect, useState } from 'react';
import {
  StatusBar, SafeAreaView, StyleSheet, View,
} from 'react-native';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Icon } from 'react-native-elements';
import Mixpanel from 'react-native-mixpanel';
import ListLayout from '../templates/ListLayout';
import Faker from '../../faker/heyirys_covid.json';
import CategoryDescription from '../molecules/CategoryDescription';
import CategoryDetailList from '../organisms/CategoryDetailList';
import Text from '../atoms/Text';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  menu: {
    marginRight: 10,
  },
});

const Categories = ({ navigation }) => {
  const { t } = useTranslation('categories');

  const { categories } = Faker;
  const mapCategories = ([id, content]) => ({ id, ...content });
  const sortCategories = (c1, c2) => c1.order - c2.order;
  const onCategoryPress = (id) => {
    Mixpanel.trackWithProperties('Selected Category', { id });
    if (categories[id].subcategories) {
      navigation.navigate({ name: 'SubCategories', key: id, params: { path: id } });
    } else if (categories[id].requireDescription) {
      navigation.navigate({ name: 'DescriptionForm', params: { path: id } });
    } else {
      navigation.navigate({ name: 'MapForm', params: { path: id } });
    }
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => <Text bold>{t('Category')}</Text>,
      headerRight: () => <Icon onPress={() => navigation.openDrawer()} name="menu" style={styles.menu} />,
    });
  }, [navigation, t]);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <ListLayout
        head={<CategoryDescription title={t('Category')} detail={t('Select An Option')} />}
        content={(
          <CategoryDetailList
            data={Object.entries(categories).map(mapCategories).sort(sortCategories)}
            onCategoryPress={onCategoryPress}
          />
        )}
      />
    </SafeAreaView>
  );
};

Categories.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    setOptions: PropTypes.func.isRequired,
    openDrawer: PropTypes.func.isRequired,
  }).isRequired,
};

export default Categories;
