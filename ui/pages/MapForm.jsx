import React, { useState, useEffect, useLayoutEffect } from 'react';
import { StatusBar, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Mixpanel from 'react-native-mixpanel';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { useTranslation } from 'react-i18next';
import { Icon } from 'react-native-elements';
import GetLocation from 'react-native-get-location';
import AbsoluteLayout from '../templates/AbsoluteLayout';
import DetailAbsoluteForm from '../molecules/DetailAbsoluteForm';
import Input from '../atoms/Input';
import useAddressApi from '../../hooks/google';
import Text from '../atoms/Text';
import SearchLocationInput from '../molecules/SearchLocationInput';

const styles = StyleSheet.create({
  mapView: {
    flexGrow: 1,
  },
  menu: {
    marginRight: 10,
  },
});

const SubCategories = ({ navigation, route }) => {
  const { t } = useTranslation(['mapform', 'shared']);
  const [mapRegion, setMapRegion] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });
  const [usingMapRegion, setUsingMapRegion] = useState(true);
  const [address, setAddress] = useState('');
  const [mapRef, setMapRef] = useState(null);
  const [getUbication, { loading, location, error }] = useAddressApi();

  const { path, detail } = route.params;
  const onRegionChange = ({ latitude, longitude }) => {
    if (!usingMapRegion) setUsingMapRegion(true);
    setMapRegion({ latitude, longitude });
  };
  const onFormSubmit = () => {
    const current = {
      latitude: usingMapRegion ? mapRegion.latitude : location.latitude,
      longitude: usingMapRegion ? mapRegion.longitude : location.latitude,
    };
    const reqData = {
      path,
      detail,
      location: current,
    };
    console.log(reqData);
    // TODO: do some awesome http request / graphql with data
    Mixpanel.trackWithProperties('Map Form', { path, 'Using Location': true });
  };
  const onFormSkip = () => {
    const reqData = {
      path,
      detail,
    };
    console.log(reqData);
    // TODO: do some awesome http request / graphql with data
    Mixpanel.trackWithProperties('Map Form', { path, 'Using Location': false });
  };

  const onAddressChange = (value) => {
    getUbication(value);
    setAddress(value);
  };

  const onUserLocation = async () => {
    try {
      const { latitude, longitude } = await GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
      });

      if (!usingMapRegion) setUsingMapRegion(true);
      setMapRegion({ latitude, longitude });
      mapRef.animateCamera({
        center: {
          latitude,
          longitude,
        },
      }, { duraiton: 500 });
    } catch (e) {
      console.log(e);
    }
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: () => <Text bold>{t('Map Form')}</Text>,
      headerRight: () => <Icon onPress={() => navigation.openDrawer()} name="menu" style={styles.menu} />,
      headerBackTitle: null,
    });
  }, [navigation, t]);

  useEffect(() => {
    if (!loading && !error && mapRef && location.latitude !== 0 && location.longitude !== 0) {
      setUsingMapRegion(false);
      mapRef.animateCamera({
        center: {
          latitude: location.latitude,
          longitude: location.longitude,
        },
      }, { duraiton: 500 });
    }
  }, [loading, error, mapRef, location.latitude, location.longitude]);

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <AbsoluteLayout
        head={(
          <SearchLocationInput
            onAddressChange={onAddressChange}
            inputPlaceholder={t('Address Placeholder')}
            address={address}
            onUserLocation={onUserLocation}
          />
        )}
        content={(
          <MapView
            ref={setMapRef}
            style={[styles.mapView]}
            provider={PROVIDER_GOOGLE}
            initialRegion={mapRegion}
            onRegionChangeComplete={onRegionChange}
          >
            <Marker
              coordinate={usingMapRegion ? mapRegion : location}
              title={t('Issue Location')}
            />
          </MapView>
        )}
        bottom={(
          <DetailAbsoluteForm
            submitLabel={t('shared:Next')}
            skipLabel={t('shared:Skip')}
            onSubmit={onFormSubmit}
            onSkip={onFormSkip}
            detail={t('Location Disclosure')}
          />
        )}
      />
    </>
  );
};

SubCategories.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
      path: PropTypes.string.isRequired,
      detail: PropTypes.string,
    }).isRequired,
  }).isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    setOptions: PropTypes.func.isRequired,
    openDrawer: PropTypes.func.isRequired,
  }).isRequired,
};

export default SubCategories;
