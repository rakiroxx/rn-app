/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';

import ChangeLanguage from './ChangeLanguage';

const CustomDrawerContent = (props) => (
  <DrawerContentScrollView {...props}>
    <DrawerItemList {...props} />
    <ChangeLanguage />
  </DrawerContentScrollView>
);

export default CustomDrawerContent;
