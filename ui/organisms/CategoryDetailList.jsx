import React from 'react';
import {
  FlatList, StyleSheet, View,
} from 'react-native';
import PropTypes from 'prop-types';
import CategoryItem from '../molecules/CategoryItem';
import useCurrentLang from '../../hooks/useCurrentLang';

const styles = StyleSheet.create({
  separator: {
    width: '90%',
    height: 2,
    backgroundColor: '#30303044',
    alignSelf: 'center',
  },
});

const CategoryDetailList = ({
  data, onCategoryPress,
}) => {
  const currentLang = useCurrentLang();

  return (
    <>
      <FlatList
        data={data}
        keyExtractor={({ id }) => id}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        renderItem={({ item }) => (
          <CategoryItem
            base64Icon={item.icon}
            categoryName={item.name[currentLang]}
            onPress={() => onCategoryPress(item.id)}
          />
        )}
      />
    </>
  );
};
CategoryDetailList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
  })).isRequired,
  onCategoryPress: PropTypes.func.isRequired,
};

export default CategoryDetailList;
