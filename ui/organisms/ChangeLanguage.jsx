import React from 'react';
import { View, StyleSheet } from 'react-native';
import Flag from 'react-native-flags';
import { useTranslation } from 'react-i18next';
import { TouchableOpacity } from 'react-native-gesture-handler';
import CustomText from '../atoms/Text';

const styles = StyleSheet.create({
  container: {
    paddingLeft: 10,
  },
  flagContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    paddingTop: 5,
  },
});

const ChangeLanguage = () => {
  const { t, i18n } = useTranslation('shared');

  return (
    <View style={styles.container}>
      <CustomText>{t('Choose Language')}</CustomText>
      <View style={styles.flagContainer}>
        <TouchableOpacity onPress={() => i18n.changeLanguage('es')}>
          <Flag
            code="MX"
            size={32}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => i18n.changeLanguage('en')}>
          <Flag
            code="US"
            size={32}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ChangeLanguage;
