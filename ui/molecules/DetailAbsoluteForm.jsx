import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Text from '../atoms/Text';
import Button from '../atoms/Button';


const styles = StyleSheet.create({
  button: {
    width: '80%',
    paddingBottom: 10,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailContainer: {
    backgroundColor: '#ffffff',
    paddingVertical: 20,
    paddingHorizontal: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const DetailAbsoluteForm = ({
  submitLabel, skipLabel, onSubmit, onSkip, disabled, detail,
}) => (
  <View style={styles.container}>
    <View style={styles.button}>
      <Button label={skipLabel} round color="#ff9922" disabled={disabled} onPress={onSkip} />
      <Button label={submitLabel} round color="#229922" disabled={disabled} onPress={onSubmit} />
    </View>
    <View style={styles.detailContainer}>
      <Text size="s">{detail}</Text>
    </View>
  </View>
);

DetailAbsoluteForm.propTypes = {
  disabled: PropTypes.bool,
  submitLabel: PropTypes.string.isRequired,
  skipLabel: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSkip: PropTypes.func.isRequired,
  detail: PropTypes.string.isRequired,
};

DetailAbsoluteForm.defaultProps = {
  disabled: false,
};

export default DetailAbsoluteForm;
