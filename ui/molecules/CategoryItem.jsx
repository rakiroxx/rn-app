import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { Icon } from 'react-native-elements';
import CustomIcon from '../atoms/CustomIcon';
import Text from '../atoms/Text';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 8,
    paddingVertical: 20,
  },
  containerText: {
    justifyContent: 'flex-start',
    flexGrow: 1,
    paddingLeft: 30,
  },
});

const CategoryItem = ({ base64Icon, categoryName, onPress }) => (
  <TouchableOpacity onPress={onPress} activeOpacity={0.4}>
    <View style={styles.container}>
      <CustomIcon base64Icon={base64Icon} />
      <View style={styles.containerText}>
        <Text>{categoryName}</Text>
      </View>
      <Icon
        name="keyboard-arrow-right"
        color="#555555"
      />
    </View>
  </TouchableOpacity>
);

CategoryItem.propTypes = {
  base64Icon: PropTypes.string,
  categoryName: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

CategoryItem.defaultProps = {
  base64Icon: null,
};

export default CategoryItem;
