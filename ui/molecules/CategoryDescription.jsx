import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Text from '../atoms/Text';

const styles = StyleSheet.create({
  title: {
    paddingBottom: 15,
  },
  detail: {
  },
});

const CategoryDescription = ({ title, detail }) => (
  <>
    <View style={styles.title}>
      <Text bold size="xl">{title}</Text>
    </View>
    <View style={styles.detail}>
      <Text>{detail}</Text>
    </View>
  </>
);

CategoryDescription.propTypes = {
  title: PropTypes.string.isRequired,
  detail: PropTypes.string.isRequired,
};
export default CategoryDescription;
