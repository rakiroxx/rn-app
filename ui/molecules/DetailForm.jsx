import React, { useState } from 'react';
import { View } from 'react-native';
import { Input } from 'react-native-elements';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Button from '../atoms/Button';

const DetailForm = ({
  onChangeText, placeholder, buttonLabel, onButtonPress, value, limitChars,
}) => {
  const { t } = useTranslation('');
  const [leftChars, setLeftChars] = useState(limitChars);
  const buttonDisable = leftChars < 0;
  const errorStyle = { color: buttonDisable ? 'red' : 'green' };
  return (
    <View style={{ flexGrow: 1 }}>
      <Input
        placeholder={placeholder}
        multiline
        value={value}
        onChangeText={(text) => {
          if (limitChars) setLeftChars(limitChars - text.length);
          onChangeText(text);
        }}
        errorMessage={limitChars && t('Chars Left', { count: leftChars })}
        errorStyle={limitChars && errorStyle}
        maxLength={limitChars}
      />
      <Button label={buttonLabel} onPress={onButtonPress} disabled={buttonDisable} />
    </View>
  );
};

DetailForm.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  buttonLabel: PropTypes.string.isRequired,
  onButtonPress: PropTypes.func.isRequired,
  limitChars: PropTypes.number,
};

DetailForm.defaultProps = {
  limitChars: null,
};

export default DetailForm;
