import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import PropTypes from 'prop-types';
import Input from '../atoms/Input';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  input: {
    width: '75%',
    marginRight: 15,
    paddingHorizontal: 20,
  },
});

const SearchLocationInput = ({
  onAddressChange, inputPlaceholder, address, onUserLocation,
}) => (
  <View style={styles.container}>
    <Input
      placeholder={inputPlaceholder}
      value={address}
      onChangeText={onAddressChange}
      containerStyle={styles.input}
    />
    <Icon onPress={onUserLocation} name="my-location" raised />
  </View>
);

SearchLocationInput.propTypes = {
  inputPlaceholder: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  onAddressChange: PropTypes.func.isRequired,
  onUserLocation: PropTypes.func.isRequired,
};

export default SearchLocationInput;
