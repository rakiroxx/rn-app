import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  headContainer: {
    paddingVertical: 40,
    paddingHorizontal: 20,
  },
  content: {
    flex: 1,
  },
});

const ListLayout = ({ head, content }) => (
  <>
    <View style={styles.headContainer}>
      {head}
    </View>
    <View style={styles.content}>
      {content}
    </View>
  </>
);

ListLayout.propTypes = {
  head: PropTypes.node.isRequired,
  content: PropTypes.node.isRequired,
};
export default ListLayout;
