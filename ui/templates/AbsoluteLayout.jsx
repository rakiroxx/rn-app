import React from 'react';
import { View, StyleSheet, SafeAreaView } from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  safeAreaView: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    justifyContent: 'space-between',
  },
  head: {
    paddingTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const AbsolutLayout = ({ head, bottom, content }) => (
  <View style={styles.container}>
    {content}
    <SafeAreaView style={styles.safeAreaView} pointerEvents="box-none">
      <View style={styles.head}>
        {head}
      </View>
      {bottom}
    </SafeAreaView>
  </View>
);

AbsolutLayout.propTypes = {
  head: PropTypes.node.isRequired,
  bottom: PropTypes.node.isRequired,
  content: PropTypes.node.isRequired,
};

export default AbsolutLayout;
