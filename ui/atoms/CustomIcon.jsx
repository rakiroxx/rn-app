import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Icon } from 'react-native-elements';
import PropTypes from 'prop-types';


const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
  image: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  icon: {
    margin: 0,
  },
});

const CustomIcon = ({ base64Icon }) => (
  <View style={styles.container}>
    {
          base64Icon ? (
            <Image
              style={styles.image}
              source={{ uri: base64Icon }}
            />
          )
            : (
              <Icon
                size={40}
                name="info"
                color="#555555"
              />
            )
        }
  </View>
);

CustomIcon.propTypes = {
  base64Icon: PropTypes.string,
};

CustomIcon.defaultProps = {
  base64Icon: null,
};

export default CustomIcon;
