import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Input } from 'react-native-elements';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    width: '85%',
    backgroundColor: '#ffffff',
    borderRadius: 45,
    paddingHorizontal: 15,
    paddingTop: 14,
  },
});

const CustomInput = ({ containerStyle, ...props }) => (
  <View style={[styles.container, containerStyle]}>
    <Input
      {...props}
    />
  </View>
);

CustomInput.propTypes = {
  containerStyle: PropTypes.shape({}),
};

CustomInput.defaultProps = {
  containerStyle: null,
};

export default CustomInput;
