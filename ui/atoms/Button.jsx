import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Text from './Text';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff4444',
    flexGrow: 1,
    margin: 5,
    maxHeight: 60,
  },
  disabled: {
    backgroundColor: '#333333',
  },
  round: {
    borderRadius: 45,
    padding: 16,
  },
});

const Button = ({
  label, onPress, disabled, round, color,
}) => {
  const buttonStyle = [styles.container];
  if (disabled) buttonStyle.push(styles.disabled);
  if (round) buttonStyle.push(styles.round);
  if (color) buttonStyle.push({ backgroundColor: color });
  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle} activeOpacity={0.4} disabled={disabled}>
      <Text bold size="l" color="#eeeeee">{label}</Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  label: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  round: PropTypes.bool,
  color: PropTypes.string,
};

Button.defaultProps = {
  disabled: false,
  round: false,
  color: '#ff4444',
};

export default Button;
