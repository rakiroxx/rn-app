import React from 'react';
import { Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

// TODO: Add Weight Types
// TODO: Add Font Sizes
// TODO: Add Font Family
const styles = StyleSheet.create({
  bold: {
    fontWeight: '700',
  },
});

const fontSizes = {
  xs: 10,
  s: 12,
  m: 15,
  l: 18,
  xl: 22,
};

const CustomText = ({
  children, bold, size, color,
}) => {
  const textStyle = [{ fontSize: fontSizes[size] }, { color }];

  if (bold) textStyle.push(styles.bold);

  return (
    <>
      <Text style={textStyle}>{children}</Text>
    </>
  );
};

CustomText.propTypes = {
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
  bold: PropTypes.bool,
  size: PropTypes.oneOf(['xs', 's', 'm', 'l', 'xl']),
};

CustomText.defaultProps = {
  color: '#aaaaaa',
  bold: false,
  size: 'm',
};

export default CustomText;
