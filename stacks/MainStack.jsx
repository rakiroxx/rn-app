import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Categories from '../ui/pages/Categories';
import SubCategories from '../ui/pages/SubCategories';
import DescriptionForm from '../ui/pages/DescriptionForm';
import MapForm from '../ui/pages/MapForm';

const Stack = createStackNavigator();

const MainStack = () => (
  <Stack.Navigator initialRouteName="Categories">
    <Stack.Screen name="Categories" component={Categories} />
    <Stack.Screen name="SubCategories" component={SubCategories} />
    <Stack.Screen name="DescriptionForm" component={DescriptionForm} />
    <Stack.Screen name="MapForm" component={MapForm} />
  </Stack.Navigator>
);

export default MainStack;
