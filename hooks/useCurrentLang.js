import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const useCurrentLang = () => {
  const { i18n } = useTranslation();
  const [currentLang, setCurrentLang] = useState(i18n.language);

  useEffect(() => {
    const onLanguageChanged = (lang) => {
      setCurrentLang(lang);
    };
    i18n.on('languageChanged', onLanguageChanged);
    return () => {
      i18n.off('languageChanged', onLanguageChanged);
    };
  }, []);

  return currentLang;
};

export default useCurrentLang;
