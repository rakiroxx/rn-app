import { useState } from 'react';
import camelize from 'camelize';

const API = 'https://maps.googleapis.com/maps/api/geocode/json';
const API_KEY = 'AIzaSyCREQdsa5xxZhiL6IHSfqJGgRMlMgWcUA8';
const delayRequestMs = 1200;

const useAddressApi = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [currentTimerId, setTimerId] = useState(null);
  const [location, setLocation] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
    formattedAddress: '',
  });

  const getUbication = (address) => {
    setLoading(true);
    if (currentTimerId) {
      clearTimeout(currentTimerId);
    }
    const timerId = setTimeout(async () => {
      try {
        const { results } = await (await fetch(`${API}?address=${address}&key=${API_KEY}`)).json();
        const { formattedAddress, accessPoints } = camelize(results[0]) || {};
        setLocation({ ...location, ...accessPoints[0].location, formattedAddress });
      } catch (e) {
        setError(e);
      } finally {
        clearTimeout(currentTimerId);
        setLoading(false);
      }
    }, delayRequestMs);
    setTimerId(timerId);
  };
  return [getUbication, { loading, error, location }];
};

export default useAddressApi;
