import React from 'react';
import './i18n';
import { NavigationContainer } from '@react-navigation/native';
import Mixpanel from 'react-native-mixpanel';
import Routes from './Routes';

Mixpanel.sharedInstanceWithToken('b7a7a5444b7ec36243e49c93c9641302');


const App = () => (
  <NavigationContainer>
    <Routes />
  </NavigationContainer>
);

export default App;
