import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import es from './locales/es';
import en from './locales/en';

const resources = {
  es,
  en,
};

i18n.use(initReactI18next).init({
  fallbackLng: 'es',
  resources,
  lng: 'es',
  ns: Object.keys(es),
  defaultNS: 'shared',
  keySeparator: false,
  interpolation: {
    escapeValue: false,
  },
  react: {
    useSuspense: false,
  },
});

export default i18n;
