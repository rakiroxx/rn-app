import shared from './shared.json';
import categories from './categories.json';
import subcategories from './subcategories.json';
import descriptionform from './descriptionform.json';
import mapform from './mapform.json';

export default {
  shared,
  categories,
  subcategories,
  descriptionform,
  mapform,
};
